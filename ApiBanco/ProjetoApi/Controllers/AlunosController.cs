﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProjetoApi.Models;

namespace ProjetoApi.Controllers
{
    public class AlunosController : ApiController
    {
        private AlunoEntities db = new AlunoEntities();



        /// <summary>
        /// Mostrar todos Alunos
        /// </summary>
        /// <returns></returns>
        // GET: api/Alunos
        public IQueryable<Aluno> GetAlunos()
        {
            return db.Alunos;
        }

        /// <summary>
        /// Mostrar um Aluno
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Alunos/5
        [ResponseType(typeof(Aluno))]
        public IHttpActionResult GetAluno(Guid id)
        {
            Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return NotFound();
            }

            return Ok(aluno);
        }

        /// <summary>
        /// Atualizar Aluno
        /// </summary>
        /// <param name="id"></param>
        /// <param name="aluno"></param>
        /// <returns></returns>
        // PUT: api/Alunos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAluno(Guid id, Aluno aluno)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aluno.ID)
            {
                return BadRequest();
            }

            db.Entry(aluno).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlunoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Criar Aluno
        /// </summary>
        /// <param name="aluno"></param>
        /// <returns></returns>
        // POST: api/Alunos
        [ResponseType(typeof(Aluno))]
        public IHttpActionResult PostAluno(Aluno aluno)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Alunos.Add(aluno);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AlunoExists(aluno.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = aluno.ID }, aluno);
        }

        /// <summary>
        /// Excluir Aluno
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Alunos/5
        [ResponseType(typeof(Aluno))]
        public IHttpActionResult DeleteAluno(Guid id)
        {
            Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return NotFound();
            }

            db.Alunos.Remove(aluno);
            db.SaveChanges();

            return Ok(aluno);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AlunoExists(Guid id)
        {
            return db.Alunos.Count(e => e.ID == id) > 0;
        }
    }
}